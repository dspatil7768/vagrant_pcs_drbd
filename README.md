# vagrant_pcs_drbd
Setting up PCS High Availability Cluster using Vagrant (DRBD Mirrored storage) 

1. Clone the repository
2. You will  need vagrant executable (For CentOS https://releases.hashicorp.com/vagrant/2.2.15/vagrant_2.2.15_x86_64.rpm)

```
sudo vagrant up
```
3. Steps to check correct installation
```
sudo vagrant ssh master01
sudo pcs status
sudo drbdadm status metabeegfs
```
4. Expected output
```
[vagrant@master01 ~]$ sudo pcs status
Cluster name: BeeGFS_meta
Cluster Summary:
  * Stack: corosync
  * Current DC: master1 (version 2.0.4-6.el8_3.1-2deceaa3ae) - partition with quorum
  * Last updated: Thu Apr  8 20:00:40 2021
  * Last change:  Thu Apr  8 19:37:28 2021 by root via cibadmin on master1
  * 2 nodes configured
  * 4 resource instances configured

Node List:
  * Online: [ master1 master2 ]

Full List of Resources:
  * vip_meta    (ocf::heartbeat:IPaddr2):        Started master1
  * Clone Set: drbd_fs-clone [drbd_fs] (promotable):
    * Masters: [ master1 ]
    * Slaves: [ master2 ]
  * beegfs      (ocf::heartbeat:Filesystem):     Started master1

Daemon Status:
  corosync: active/disabled
  pacemaker: active/disabled
  pcsd: active/enabled
```
```
[vagrant@master01 ~]$ sudo drbdadm status metabeegfs
metabeegfs role:Primary
  disk:UpToDate
  master02 role:Secondary
    peer-disk:UpToDate
```
drbdtop output:
```
DRBDTOP  (kernel: 9.0.25; utils: 9.13.1; host: master01)
┌◉ (LIVE UPDATING) Resource List────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│  Name       | Role    | Disks | Peer Disks | Connections | Overall | Quorum                                                                                                           │
│  metabeegfs | Primary | ✓     | ✓          | ✓           | ✓       | ✓                                                                                                                │
│                                                                                                                                                                                      │
│                                                                                                  
│                                                                                                                                                                                       │
│                                                                                                  
└───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
q: QUIT | j/k: down/up | f: Toggle dangerous filter | <tab>: Toggle updates
```
