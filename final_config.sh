#!/bin/bash

no_host_key_check="-o StrictHostKeyChecking=no"
export SSHPASS='vagrant'

# Checking hostname on second master
echo "##################### Checking master2 status..... #######################################"
sshpass -e ssh $no_host_key_check vagrant@master2 'hostname'

# Installing REPO rpm
sudo dnf -y install https://www.elrepo.org/elrepo-release-8.el8.elrepo.noarch.rpm
sudo dnf config-manager --set-enabled ha

# Installing above on master2
echo "##################### Installing repo and enabling HA repo on master2..... #######################################"
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo dnf -y install https://www.elrepo.org/elrepo-release-8.el8.elrepo.noarch.rpm'
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo dnf config-manager --set-enabled ha'

# Installing key
sudo rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org

# Installing key on master2
echo "##################### Installing key on master2..... #######################################"
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org'

# Installing pcs & drbd9 packages
sudo dnf -y install vim drbd90-utils kmod-drbd90
sudo dnf -y install pcs fence-agents-all pcp-zeroconf pacemaker psmisc 

# Installing pcs & drbd9 packages on master2
echo "##################### Installing DRBD and PCS packages on master2..... #######################################"
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo dnf -y install vim drbd90-utils kmod-drbd90'
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo dnf -y install pcs fence-agents-all pcp-zeroconf'

# Configuring password for hacluster user
echo "123456" | passwd hacluster --stdin

# Configuring password for hacluster user on master2
echo "##################### Configuring password for hacluster user on master2..... #######################################"
sshpass -e ssh $no_host_key_check vagrant@master2 'echo "123456" | sudo passwd hacluster --stdin'

# Starting PCS services
sudo systemctl start pcsd.service
sudo systemctl enable pcsd.service

# Starting PCS services on master2
echo "##################### Starting and enabling pcs service on master2..... #######################################"
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo systemctl start pcsd.service && sudo systemctl enable pcsd.service'

# Setting up cluster
sudo pcs host auth master1 master2 -u hacluster -p 123456
sudo pcs cluster setup BeeGFS_meta --start master1 master2
sudo pcs cluster start --all

# Configuring pcs alerts
install --mode=0755 /usr/share/pacemaker/alerts/alert_file.sh.sample /var/lib/pacemaker/alert_file.sh
touch /var/log/pcmk_alert_file.log
chown hacluster:haclient /var/log/pcmk_alert_file.log
chmod 600 /var/log/pcmk_alert_file.log

# Configuring pcs alerts on master2
echo "##################### Installing alerts files on master2..... #######################################"
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo install --mode=0755 /usr/share/pacemaker/alerts/alert_file.sh.sample /var/lib/pacemaker/alert_file.sh'
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo touch /var/log/pcmk_alert_file.log'
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo chown hacluster:haclient /var/log/pcmk_alert_file.log'
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo chmod 600 /var/log/pcmk_alert_file.log'
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo pcs alert create id=alert_file description="Log events to a file." path=/var/lib/pacemaker/alert_file.sh'
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo pcs alert recipient add alert_file id=my-alert_logfile value=/var/log/pcmk_alert_file.log'
echo "Alerts configured successfully. Check /var/log/pcmk_alert_file.log for logs..."

# Installing some packages
sudo dnf -y install policycoreutils-python-utils
sudo semanage permissive -a drbd_t

# Installing some packages on master2
echo "##################### Installing some required packages on master2..... #######################################"
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo dnf -y install policycoreutils-python-utils lvm2'
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo semanage permissive -a drbd_t'

# Configuring LVM
sudo pvcreate /dev/vdc && sudo vgcreate drbdpool /dev/vdc && sudo lvcreate -n drbdata -l100%FREE drbdpool

# configuring LVM on master2
echo "##################### Configuring LVM on master2..... #######################################"
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo pvcreate /dev/vdc && sudo vgcreate drbdpool /dev/vdc && sudo lvcreate -n drbdata -l100%FREE drbdpool'


# Configuring drbd.res file
sudo cat <<EOT > /etc/drbd.d/metabeegfs.res
resource metabeegfs {
  on master01 {
    device    /dev/drbd1;
    disk      /dev/mapper/drbdpool-drbdata;
    address   192.168.18.9:7789;
    meta-disk internal;
  }
  on master02 {
    device    /dev/drbd1;
    disk      /dev/mapper/drbdpool-drbdata;
    address   192.168.18.10:7789;
    meta-disk internal;
  }
}
EOT

# Configuring drbd.res file on master2
echo "##################### Configuring drbd.res file on master2..... #######################################"
sshpass -e scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -r /etc/drbd.d/metabeegfs.res root@master2:/etc/drbd.d/metabeegfs.res

# Configuring global_common.conf file
sudo cat <<EOT > /etc/drbd.d/global_common.conf
global {
 usage-count no;
}
common {
  options {
    auto-promote yes;
 }
 net {
  protocol C;
 }
}
EOT

# Configuring global_common.conf file on master2
echo "##################### Configuring global_common.conf file on master2..... #######################################"
sshpass -e scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -r /etc/drbd.d/global_common.conf root@master2:/etc/drbd.d/global_common.conf

# Initialize DRDB resource
sudo drbdadm create-md metabeegfs

# Initialize DRDB resource on master2
echo "##################### Initialize DRDB resource on master2..... #######################################"
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo drbdadm create-md metabeegfs'

# Enable DRBD resource
sudo drbdadm up metabeegfs

# Enable DRBD resource on master2
echo "##################### Enable DRBD resource on master2..... #######################################"
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo drbdadm up metabeegfs'


# Checking 'drbdadm status metabeegfs'
sudo drbdadm status metabeegfs

# Initial synchronization
drbdadm primary --force metabeegfs
echo "Sleeping for 70 sec so that initial synchronisation gets complete (which is mandatory before creating FS on it)..."
sleep 80

# status drbdadm
sudo drbdadm status metabeegfs

# Downloading go package
sudo go get github.com/LINBIT/drbdtop 
sudo cp /root/go/bin/drbdtop /usr/bin/

# Downloading go package on master2
sshpass -e ssh $no_host_key_check vagrant@master2 'sudo go get github.com/LINBIT/drbdtop && sudo cp /root/go/bin/drbdtop /usr/bin/'

# Creating filesystem
sudo mkfs.ext4 /dev/drbd1


# Declaring .cib file mainly for DRBD configuration
pcs cluster cib tmp-cib.xml
cp tmp-cib.xml tmp-cib.xml.deltasrc
pcs -f tmp-cib.xml property set stonith-enabled=false
pcs -f tmp-cib.xml resource defaults update migration-threshold=1 resource-stickiness=100 startdelay=5s
pcs -f tmp-cib.xml resource create vip_meta ocf:heartbeat:IPaddr2 cidr_netmask=24 ip=192.168.18.11 nic=eth1:0
pcs -f tmp-cib.xml resource create drbd_fs ocf:linbit:drbd drbd_resource=metabeegfs promotable promoted-max=1 promoted-node-max=1 clone-max=2 clone-node-max=1 notify=true
pcs -f tmp-cib.xml resource create beegfs ocf:heartbeat:Filesystem device=/dev/drbd1 directory=/drbdfs fstype=ext4
#pcs -f tmp-cib.xml resource create metadata_symlink ocf:heartbeat:symlink link=/mnt/beegfs-meta target=/drbdfs/meta
#pcs -f tmp-cib.xml resource group add Symblinks metadata_symlink
pcs -f tmp-cib.xml constraint colocation add beegfs with master drbd_fs-clone
pcs -f tmp-cib.xml constraint colocation add master drbd_fs-clone with vip_meta
#pcs -f tmp-cib.xml constraint colocation add Symblinks with beegfs
pcs -f tmp-cib.xml constraint order set vip_meta drbd_fs-clone beegfs
#pcs -f tmp-cib.xml constraint order promote drbd_fs_clone then beegfs
pcs -f tmp-cib.xml constraint location vip_meta prefers master1=100
pcs -f tmp-cib.xml constraint location vip_meta prefers master2=50
pcs -f tmp-cib.xml constraint location drbd_fs-clone prefers master1=100
pcs cluster cib-push tmp-cib.xml diff-against=tmp-cib.xml.deltasrc
echo "CIB pushed..DRBD configured..."

